import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: {},
  },
  mutations: {
  },
  actions: {
    updateUser({ state }, payload) {
      Object.assign(state.user, payload);
    },
  },
  getters: {
    getUser(state) {
      return state.user;
    },
  },
});
