import Vue from 'vue';
import Router from 'vue-router';

// App Security
import AppSecurity from './utils/AppSecurity';

Vue.use(Router);

export default class AppRouter {
  constructor() {
    this.router = new Router({
      mode: 'history',
      base: process.env.BASE_URL,
      routes: [
        {
          path: '/',
          name: 'main',
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () => import(/* webpackChunkName: "about" */ './components/main/Main.vue'),
        },
        {
          path: '/about',
          name: 'about',
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () => import(/* webpackChunkName: "about" */ './components/About.vue'),
        },
        {
          path: '/admin',
          name: 'admin',
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () => import(/* webpackChunkName: "about" */ './components/admin/Admin.vue'),
        },
      ],
    });
    this.addOnBeforeGuard();
  }

  /**
  * Adds guards to router, it will be used to check if the user is logged in
  */
  addOnBeforeGuard() {
    this.router.beforeEach((to, from, next) => {
      console.log('before router happens');
      if (AppSecurity.hasAccess()) {
        next();
      }
    });
  }
}
