import Vue from 'vue';
import './plugins/vuetify';
import App from './App.vue';
import AppRouter from './router';
import store from './store';

Vue.config.productionTip = false;

new Vue({
  router: new AppRouter().router,
  store,
  render: h => h(App),
}).$mount('#app');
