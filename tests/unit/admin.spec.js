import { mount } from '@vue/test-utils';
import Admin from '@/components/admin/Admin.vue';
import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify';

Vue.use(Vuetify);
Vue.use(Vuex);

describe('Admin.vue', () => {
  const store = new Vuex.Store({
    state: {
      user: {},
    },
    mutations: {
    },
    actions: {
      updateUser({ state }, payload) {
        Object.assign(state.user, payload);
      },
    },
    getters: {
      getUser(state) {
        return state.user;
      },
    },
  });
  it('Initially the edit button text is Edit and has class dark', () => {
    document.body.setAttribute('data-app', true);
    const wrapper = mount(Admin, {
      store,
      Vue,
    });
    const editButton = wrapper.find('.v-btn');
    const editButtonText = wrapper.find('.v-btn__content').text();
    expect(editButtonText).toMatch('Edit');
    expect(editButton.hasClass('dark')).toBe(true);
  });
  it('When clicked edit button text is Save and has class success', () => {
    document.body.setAttribute('data-app', true);
    const wrapper = mount(Admin, {
      store,
      Vue,
    });
    const editButton = wrapper.find('.v-btn');
    editButton.trigger('click');
    const editButtonText = wrapper.find('.v-btn__content').text();
    expect(editButtonText).toMatch('Save');
    expect(editButton.hasClass('success')).toBe(true);
  });
});
